import case4 from './case-4'
import case6 from './case-6'
import case7 from './case-7'
import case89 from './case-8-9'
import case10 from './case-10'

const cases = [
  case4,
  case6,
  case7,
  case89,
  case10,
]

export default cases
