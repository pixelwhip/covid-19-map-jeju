// See: https://www.instagram.com/p/B-IwDwpDxBh/
export default {
  name: 'case-8-9',
  label: 'Cases #8 and #9',
  color: '#C05621',
  description: `Patients #8 and #9 are two Jeju residents who traveled in Spain from February 7 to March 17. They arrived at Incheon Airport on March 18 at around 6 p.m. on Qatar Airways flight QR858. They flew to Jeju on March 19 arriving at 1:59 p.m. on T’way flight TW715. One individual (case #8) developed symptoms of COVID-19 (muscle pain, cough) shortly after returning to Jeju.

    After inquiring about getting tested and seeking medical help at Cheju Halla Hospital on March 22 and March 23rd, both travellers were confirmed to have the virus on March 24. Case #8 is a teacher on Jeju Island but has not been in contact with the school or students since leaving Jeju for Spain on February 7.

    As of 8:30 p.m. on March 24th, eight sites visited by the two individuals have been subject to disinfection and sterilization protocols. Officials also confirmed that case #8 was wearing a mask while in public.`,

  activity: [
    {
      type: 'Point',
      point: [126.492969, 33.506951],
      placename: 'Jeju International Airport',
      address: '제주시 공항로 2',
      arrived: '2020-03-19 13:59+09',
      departed: '2020-03-19 14:13+09',
      note: 'Arrive Jeju Airport on T’way Air (TW715)',
    },

    {
      type: 'Point',
      point: [],
      placename: 'Private Residence',
      arrived: '2020-03-19 14:26+09',
      departed: '2020-03-22 01:10+09',
      note: 'Go home using a Jumbo Taxi. Both individuals stay there for two days.',
    },

    {
      type: 'Point',
      point: [],
      placename: 'Laundromat',
      arrived: '2020-03-22 01:10+09',
      departed: '2020-03-22 01:50+09',
      note: 'Go to laundromat',
    },

    {
      type: 'Point',
      point: [126.471994, 33.492459],
      placename: 'GS25 Jeju Wollang Branch',
      address: '제주시 월랑로 93',
      arrived: '2020-03-22 01:54+09',
      departed: '2020-03-22 01:56+09',
      note: 'Go to nearby GS25',
    },

    {
      type: 'Route',
      route: [
        [126.465975, 33.503627],
        [126.467380, 33.505049],
        [126.469032, 33.505809],
        [126.470609, 33.508663],
        [126.473731, 33.509477],
        [126.474986, 33.509334],
        [126.476316, 33.508708],
        [126.477432, 33.508726],
        [126.480736, 33.509835],
      ],
      placename: '용담해안도로',
      address: '제주시 도두일동',
      arrived: '2020-03-22 02:00+09',
      departed: '2020-03-22 03:00+09',
      note: 'Take a walk on the beach (Dodu-dong Coastal Road), use taxi to return home',
    },

    {
      type: 'Point',
      point: [],
      placename: 'Private Residence',
      arrived: '2020-03-22 03:00+09',
      departed: '2020-03-22 12:30+09',
      note: 'Remain at home for the rest of the morning.',
    },

    {
      type: 'Point',
      point: [126.486620, 33.489848],
      placename: 'GS25 Yeon-dong Shingwang Branch',
      address: '제주시 도령로 76',
      arrived: '2020-03-22 12:56+09',
      departed: '2020-03-22 13:00+09',
      note: 'Go to GS25 on the way to Cheju Halla Hospital',
    },

    {
      type: 'Point',
      point: [126.485316, 33.489685],
      placename: 'Cheju Halla Hospital',
      address: '제주시 도령로 65',
      arrived: '2020-03-22 13:05+09',
      departed: '2020-03-22 13:09+09',
      note: 'Patient #8 goes to Cheju Halla Hospital to inquire about getting an examination for Patient #9.',
    },

    {
      type: 'Point',
      point: [126.497191, 33.487257],
      placename: 'Jesco Mart',
      address: '제주시 신대로 109',
      arrived: '2020-03-22 14:05+09',
      departed: '2020-03-22 14:30+09',
      note: 'Patient #9 goes to Jesco Mart in Shin Jeju.',
    },

    {
      type: 'Point',
      point: [],
      placename: 'Private Residence',
      arrived: '2020-03-22 14:30+09',
      departed: '2020-03-23 05:08+09',
      note: 'Remain at home for the rest of the day.',
    },

    {
      type: 'Point',
      point: [126.497065, 33.491591],
      placename: 'GS25 Jeju Provincial Office Branch',
      address: '제주시 신대로 63 101~102호',
      arrived: '2020-03-23 05:08+09',
      departed: '2020-03-23 05:12+09',
      note: '(6th Jeju patient, confirmed route traveling alone) GS25 Jeju Provincial Office Branch',
    },

    {
      type: 'Point',
      point: [126.493204, 33.489651],
      placename: 'CU Shin Jeju Branch',
      address: '제주시 삼무로 62',
      arrived: '2020-03-23 10:35+09',
      departed: '2020-03-23 10:39+09',
      note: '(6th Jeju patient, confirmed route traveling alone) CU Shin Jeju Branch',
    },

    {
      type: 'Point',
      point: [126.485316, 33.489685],
      placename: 'Cheju Halla Hospital',
      address: '제주시 도령로 65',
      arrived: '2020-03-23 11:00+09',
      departed: '2020-03-23 12:00+09',
      note: 'Patient #8 returns to Cheju Halla Hospital Screening Clinic (on foot) and then returns home after examination.',
    },

    {
      type: 'Point',
      point: [],
      placename: 'Private Residence',
      arrived: '2020-03-23 12:30+09',
      note: 'Remain at home for the rest of the day. Patient #8 was confirmed to have COVID-19 the next day. They received a call from Cheju Halla Hospital at 15:15. Patient #9 was also confirmed at 19:55 by the Public Health and Environment Research Institute.',
    },

    // {
    //   type: 'Route',
    //   route: [],
    // },
  ],
}
