export default {
  name: 'case-4',
  label: 'Case #4',
  color: '#C53030',
  description: `Case #4 is a COVID-19 patient from Seoul who took a day trip
    to Jeju on March 7, 2020. It has been confirmed that a 40-year-old woman
    who went to a Public Health Center in Seoul on March 4 with COVID-19
    symptoms, later traveled to Jeju Island on a day-trip by herself on March
    7. She was confirmed to have COVID-19 upon her return to the mainland.`,

  activity: [
    {
      type: 'Point',
      point: [126.802939, 37.558863],
      placename: 'Gimpo Airport',
      address: '서울 강서구 하늘길 112',
      departed: '2020-03-07 08:45+09',
      note: 'Departs Gimpo Airport for Jeju Island on Asiana Airlines (OZ 8915)',
    },

    {
      type: 'Point',
      point: [126.492969, 33.506951],
      placename: 'Jeju International Airport',
      address: '제주시 공항로 2',
      arrived: '2020-03-07 10:00+09',
      departed: '2020-03-07 10:13+09',
      note: 'Arrives at Jeju International Airport',
    },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (10:13 ~ 10:24) Took bus 466 from Jeju International Airport',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (10:25 ~ 11:00) Ate at the Jeju Gisa Buffet Restaurant near Jeju Intercity Bus Terminal',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: △ (11:00 ~11:24) Waited outside at Jeju Intercity Bus Terminal for bus',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (11:25 ~ 13:03) Takes bus No. 201 to Gwangchigi Beach on the east of the island',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (13:03 ~ 13:32) Walks around Gwangchigi Beach',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (13:32 ~ 14:11) Gets back on bus No. 201 to go to west to Dongseong-dong in the Gimnyeong area',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (14:11 ~ 14:25)  Looks at canola flowers',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (14:25 ~ 14:39) Gets back on bus 201 to return west to Hamdeok.',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (14:39 ~ 14:50) Walks along Hamdeok Beach △ (14:50 ~ 15:00) Goes to the U-Dream Mart at Hamdeok △ (15:00 ~ 18:30) Goes to the “Poem House” Pension in Hamdeok',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: △ (18:33 ~ 18:45) Eats at the GS25 Convenience Store at the Golden Tulip Hotel in Hamdeok',
    // },

    // {
    //   type: 'Point',
    //   point: [],
    //   placename: '',
    //   address: '',
    //   arrived: '2020-03-07 0:00+09',
    //   departed: '2020-03-07 0:00+09',
    //   note: 'New: (19:07~20:04) Takes bus 331 to Jeju Airport △ (21:10) Travels from Jeju back to Gimpo Airport on Asiana Airlines (OZ 8996)',
    // },

    // {
    //   type: 'Route',
    //   route: [],
    // },
  ],
}
