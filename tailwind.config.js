module.exports = {
  theme: {
    extend: {
      spacing: {
        em: '1em',
      },
    },
  },
  variants: {},
  plugins: [],
}
